package com.emrekose.layoutlar

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)


        /*
        val linearLayout = LinearLayout(this)
        linearLayout.orientation = LinearLayout.VERTICAL

        val textView = TextView(this)
        textView.text = "Hello LinearLayout"

        linearLayout.addView(textView)
        setContentView(linearLayout)
        */

        val relativeLayout = RelativeLayout(this)
        val rlParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)

        val textView = TextView(this)
        textView.text = "Hello Relative Layout"
        textView.id = 1

        val tvParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)

        tvParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
        relativeLayout.addView(textView)

        val btnParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)

        val button = Button(this).apply {
            text = "Button 1"
            layoutParams = btnParams
        }

        btnParams.addRule(RelativeLayout.BELOW, 1)
        relativeLayout.addView(button)

        setContentView(relativeLayout, rlParams)
    }
}
