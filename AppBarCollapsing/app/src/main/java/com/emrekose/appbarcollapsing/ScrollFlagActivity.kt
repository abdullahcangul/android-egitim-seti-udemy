package com.emrekose.appbarcollapsing

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_scroll_flag.*

class ScrollFlagActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scroll_flag)

        setSupportActionBar(toolbar)
        toolbar.title = "Scroll Flag"
    }
}
