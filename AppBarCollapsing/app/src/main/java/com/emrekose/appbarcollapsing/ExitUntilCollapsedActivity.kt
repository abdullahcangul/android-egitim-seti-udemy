package com.emrekose.appbarcollapsing

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_exit_until_collapsed.*

class ExitUntilCollapsedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exit_until_collapsed)

        setSupportActionBar(toolbar)
        collapsingToolbarLayout.title = "Exit Until Collapsed"
    }
}
