package com.emrekose.fragmentmultipane.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import com.emrekose.fragmentmultipane.R

class ListFragment : Fragment() {

    private var onListItemClickListener: OnListItemClickListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        val listView: ListView = view.findViewById(R.id.listView)

        val userList = listOf("Emre", "Ali", "Ayşe", "Mehmet", "Batuhan", "Simge")
        val adapter = ArrayAdapter(activity, android.R.layout.simple_list_item_1, userList)
        listView.adapter = adapter

        listView.setOnItemClickListener { parent, view, position, id ->
            onListItemClickListener?.onItemClick(userList[position])
        }

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            onListItemClickListener = context as OnListItemClickListener
        } catch (e: Exception) {

        }
    }

    interface OnListItemClickListener {
        fun onItemClick(name: String)
    }
}
