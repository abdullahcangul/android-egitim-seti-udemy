package com.emrekose.fragments2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firstFrg = FirstFragment()
        val secondFrg = SecondFragment()
        val thirdFrg = ThirdFragment()


        add_first_frg.setOnClickListener { addFragment(firstFrg)}
        add_second_frg.setOnClickListener {addFragment(secondFrg) }
        add_third_frg.setOnClickListener {addFragment(thirdFrg) }
    }

    fun addFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }
}
