package com.emrekose.spinnerornek

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

class CountryAdapter(context: Context, @LayoutRes res: Int = 0, countryList: ArrayList<Country>):
        ArrayAdapter<Country>(context, res, countryList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View?
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_country_spinner, parent, false)
        } else {
            view = convertView
        }

        var countryName: TextView? = view?.findViewById(R.id.country_name)
        var flagImg: ImageView? = view?.findViewById(R.id.country_flag)

        var country: Country = getItem(position)

        countryName?.text = country.name
        flagImg?.setImageResource(country.flag)

        return view as View
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getView(position, convertView, parent)
    }
}