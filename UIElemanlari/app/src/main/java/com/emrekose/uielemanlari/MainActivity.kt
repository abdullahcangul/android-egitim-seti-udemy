package com.emrekose.uielemanlari

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        val TAG: String = "MainActivity"
    }

    private lateinit var textView: TextView
    private lateinit var button: Button
    private lateinit var button2: Button
    private lateinit var button3: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        textView = findViewById<TextView>(R.id.textView1)
        button = findViewById<Button>(R.id.button1)
        button2 = findViewById<Button>(R.id.button2)
        button3 = findViewById(R.id.button3)

        button3.setOnClickListener {
            textView.text = "Button 3 e tıklandı"
            Toast.makeText(this, "Button 3 e tıklandı", Toast.LENGTH_SHORT).show()
        }

        Toast.makeText(this, "Hoşgeldiniz", Toast.LENGTH_SHORT).show()

        for(i in 1..5) {
            Log.e(TAG, i.toString())
        }
    }

    override fun onClick(view: View?) {
        when(view?.id) {
            // kodlar
        }
    }

    fun onButtonClicked(view: View) {
        when(view.id) {
            R.id.button1 -> textView.text = "Butona tıklandı."
            R.id.button2 -> textView.text = "Button 2ye tıklandı"
        }
    }
}
