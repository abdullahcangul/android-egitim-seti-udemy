package com.emrekose.listviewornek

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var listView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = findViewById(R.id.listview)

        val androidVersion = listOf("Cupcake","Eclair","Froyo", "GingerBread","HoneyComb","IceCream Sandvich",
                "Jelly Bean","KitKat","Lollipop","Marsmallow","Nougat","Oreo","Pie")

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, androidVersion)

        listView.adapter = adapter

        listView.onItemClickListener = AdapterView.OnItemClickListener{ adapterView: AdapterView<*>, view1: View, position: Int, l: Long ->
            val selectedItem = adapterView.getItemAtPosition(position) as String

            Toast.makeText(this, "Seçtiğiniz version\n $selectedItem", Toast.LENGTH_SHORT).show()
        }

    }
}
