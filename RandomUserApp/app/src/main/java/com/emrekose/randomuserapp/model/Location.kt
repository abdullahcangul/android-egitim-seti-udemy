package com.emrekose.randomuserapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Location(
    @SerializedName("street")
    var street: String,

    @SerializedName("city")
    var city: String
): Serializable