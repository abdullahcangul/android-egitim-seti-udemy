package com.emrekose.retrofitgsondemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val apiClient = ApiClient.getApiClient()

        btn_getPosts.setOnClickListener {
            apiClient.getPostList().enqueue(object : Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                    Log.e("getPostList", "onFailure: ${t.message}")
                }

                override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                    for (i in 0 until response.body()!!.size) {
                        Log.d("getPostList", "onResponse: ${response.body()?.get(i)?.title}")
                    }
                }

            })
        }

        btn_getSinglePost.setOnClickListener {
            apiClient.getSinglePost(1).enqueue(object : Callback<Post> {
                override fun onFailure(call: Call<Post>, t: Throwable) {
                    Log.e("getSinglePost", "onFailure: ${t.message}")
                }

                override fun onResponse(call: Call<Post>, response: Response<Post>) {
                    Log.d("getPostList", "onResponse: ${response.body()?.title}")
                }
            })
        }

        btn_getPostwithQuery.setOnClickListener {
            apiClient.getPostwithQuery(10).enqueue(object : Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                    Log.e("getPostwithQuery", "onFailure: ${t.message}")
                }

                override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                    for (i in 0 until response.body()!!.size) {
                        Log.d("getPostwithQuery", "onResponse: ${response.body()?.get(i)?.title}")
                    }
                }

            })
        }

        btn_savePostWithFields.setOnClickListener {
            apiClient.savePostwithField("test title", "test body").enqueue(object : Callback<Post> {
                override fun onFailure(call: Call<Post>, t: Throwable) {
                    Log.e("savePostwithField", "onFailure: ${t.message}")
                }

                override fun onResponse(call: Call<Post>, response: Response<Post>) {
                    Log.d("savePostwithField", "onResponse: title = ${response.body()?.title} body = ${response.body()?.body}")
                }

            })
        }

        btn_updatePost.setOnClickListener {
            apiClient.updatePost(1, "update title", "update body").enqueue(object : Callback<Post> {
                override fun onFailure(call: Call<Post>, t: Throwable) {
                    Log.e("updatePost", "onFailure: ${t.message}")
                }

                override fun onResponse(call: Call<Post>, response: Response<Post>) {
                    Log.d("updatePost", "onResponse: title = ${response.body()?.title} body = ${response.body()?.body}")
                }
            })
        }

        btn_deletePost.setOnClickListener {
            apiClient.deletePost(2).enqueue(object : Callback<Post> {
                override fun onFailure(call: Call<Post>, t: Throwable) {
                    Log.e("deletePost", "onFailure: ${t.message}")
                }

                override fun onResponse(call: Call<Post>, response: Response<Post>) {
                    Log.d("getPostwithQuery", "onResponse: ${response.body()}")
                }

            })
        }









    }
}
