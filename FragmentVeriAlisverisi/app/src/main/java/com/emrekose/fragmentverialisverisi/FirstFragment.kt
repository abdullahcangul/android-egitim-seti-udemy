package com.emrekose.fragmentverialisverisi


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

class FirstFragment : Fragment() {

    private var firstFragmentListener: FirstFragmentListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_first, container, false)

        val btn = view.findViewById<Button>(R.id.first_fragment_btn)
        val editText = view.findViewById<EditText>(R.id.first_fragment_edt)

        btn.setOnClickListener {
            val message: String = editText.text.toString()
            firstFragmentListener?.messageFromFirst(message)
        }

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try{
            firstFragmentListener = context as FirstFragmentListener
        }catch (e: ClassCastException) {
            throw java.lang.ClassCastException(context.toString() + " FirstFragmentListener implement edilmeli")
        }
    }

    override fun onDetach() {
        super.onDetach()
        firstFragmentListener = null
    }


}
