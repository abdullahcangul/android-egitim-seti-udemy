package com.emrekose.fragmentverialisverisi

interface FirstFragmentListener {
    fun messageFromFirst(message: String)
}