package com.emrekose.tablayoutdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: TabAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = TabAdapter(supportFragmentManager)
        adapter.apply {
            addFragment(FirstFragment(), "Tab1")
            addFragment(SecondFragment(), "Tab2")
            addFragment(ThirdFragment(), "Tab3")
        }

        viewpager.adapter = adapter
        tabLayout.setupWithViewPager(viewpager)

        tabLayout.getTabAt(0)?.setIcon(R.drawable.ic_home)
        tabLayout.getTabAt(1)?.setIcon(R.drawable.ic_person)
        tabLayout.getTabAt(2)?.setIcon(R.drawable.ic_settings)
    }
}
