package com.emrekose.bottomnav

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_nav.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.action_home -> {
                    fragmentTransact(HomeFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_discover -> {
                    fragmentTransact(DiscoverFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_person -> {
                    fragmentTransact(ProfileFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                else -> TODO()
            }
        }
    }

    private fun fragmentTransact(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }
}
