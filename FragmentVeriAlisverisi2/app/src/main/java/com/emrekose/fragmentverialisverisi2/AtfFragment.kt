package com.emrekose.fragmentverialisverisi2


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class AtfFragment : Fragment() {

    companion object {
        fun newInstance(message: String): AtfFragment {
            val args = Bundle()
            args.putString("atf_msg", message)

            val fragment = AtfFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_atf, container, false)

        val message = arguments?.getString("atf_msg")
        view.findViewById<TextView>(R.id.atf_tv).text = message

        return view
    }


}
