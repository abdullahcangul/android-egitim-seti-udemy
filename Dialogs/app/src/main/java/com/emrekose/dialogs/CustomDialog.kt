package com.emrekose.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.EditText

class CustomDialog: DialogFragment() {

    private lateinit var userName: EditText
    private lateinit var password: EditText

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val view: View? = activity?.layoutInflater?.inflate(R.layout.custom_dialog, null)
        builder.setView(view)
            .setPositiveButton("Login", {dialog, which ->

            })
            .setNegativeButton("Cancel", {dialog, which ->

            })

        userName = view!!.findViewById(R.id.dialog_username)
        password = view!!.findViewById(R.id.dialog_password)

        return builder.create()
    }
}