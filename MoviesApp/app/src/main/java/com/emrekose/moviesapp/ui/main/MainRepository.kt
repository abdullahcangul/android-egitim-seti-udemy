package com.emrekose.moviesapp.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.emrekose.moviesapp.data.remote.ApiClient
import com.emrekose.moviesapp.data.remote.ApiService
import com.emrekose.moviesapp.model.movie.MovieResponse
import com.emrekose.moviesapp.model.movie.MovieResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainRepository {
    private val apiService: ApiService by lazy { ApiClient.getApiService() }

    fun getPopularMovies(): LiveData<List<MovieResults>>? {
        val moviesLiveData: MutableLiveData<List<MovieResults>> = MutableLiveData()
        apiService.getPopularMovies().enqueue(object : Callback<MovieResponse> {
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.e("getPopularMovies",  t.message)
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                moviesLiveData.value = response.body()?.results
            }

        })

        return moviesLiveData
    }

    fun getTopRatedMovies() {

    }
}