package com.emrekose.recyclerviewdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnUserClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var userList = arrayListOf<User>(
            User("Celina Foster", R.drawable.user1),
            User("Daniel Richardson", R.drawable.user2),
            User("Adrian Nichols", R.drawable.user3),
            User("Leah Lane", R.drawable.user4),
            User("Rosa Duncan", R.drawable.user5),
            User("Benjamin Stephens", R.drawable.user6),
            User("Pamela Cooper", R.drawable.user7),
            User("Roy Stanley", R.drawable.user8),
            User("Lonnie Griffin", R.drawable.user9),
            User("Darren Ferguson", R.drawable.user10)
        )

        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.adapter = UserRecyclerViewAdapter(userList, this)
    }

    override fun onUserClick(user: User) {
        Toast.makeText(this, user.name, Toast.LENGTH_LONG).show()
    }
}
